/* Global shared variable */
var browser = browser || chrome;
var hide_number = false;
var balance = {};
var popup = null;
var total = 0;
var total_hour = 0;
var total_day = 0;
var total_week = 0;

var currency_keys = {};
var configured = false;
var update_time;
var currency_values = {};
var last_hour = {};
var last_day = {};
var last_week = {};
var currency;
var tickervalue;
var bg_timer = null;

var server_time = (new Date()).getTime() / 1000;

var last_update = new Date();

let version = '0';
let url = 'https://api.kraken.com';

let api_key = '';
let private_key = '';

browser.alarms.onAlarm.addListener(run);
browser.alarms.create('update', {periodInMinutes: 5 });


function getNonce()
{
    let date = (new Date()).getTime()+'';
    let rand1 = Math.ceil(Math.random()* 100000)+'';
    let rand2 = Math.ceil(Math.random()* 100000)+'';

    let noncet = date+rand1+rand2+rand1+rand2;
    return (noncet).substr(0,16);
}

function getConf()
{
    var browser = browser || chrome
    var g1 = new Promise(function(ok,  reject)
    {
        browser.storage.local.get("api_key",function(result)
        {
            if(!result || !result.api_key) { return onError(); }
            api_key = result.api_key;
            ok();
        });
    });

    var g2 = new Promise(function(ok,  reject)
    {
        browser.storage.local.get("private_key", function(result)
        {
            if(!result || !result.private_key) { return onError(); }
            private_key = result.private_key;
            ok();
        });
    });

    var g3 = new Promise(function(ok,  reject)
    {
        browser.storage.local.get("currency", function(result)
        {
            if(!result || !result.currency) { return onError(); }
            currency = result.currency;
            currency_values[currency+currency] = 1;
            currency_keys[currency+currency] = currency+currency;
            last_day[currency+currency] = 1;
            last_hour[currency+currency] = 1;
            last_week[currency+currency] = 1;
            ok();
        });
    });
    var g4 = new Promise(function(ok,  reject)
    {
        browser.storage.local.get("hide_number", function(result)
        {
            if(!result) { return onError(); }
            hide_number = result.hide_number;
            ok();
        });
    });
    var g5 = new Promise(function(ok,  reject)
    {
        browser.storage.local.get("tickervalue", function(result)
        {
            if(!result) { return onError(); }
            tickervalue = result.tickervalue;
            ok();
        });
    });

    return Promise.all([g1, g2, g3, g4, g5]);
};

function onError()
{
}

function QueryPrivate(method, post_data)
{
    return new Promise(function(ok, reject)
    {
        if(!private_key || !api_key || !currency)
        {
            console.error('NO API OR PRIVATE KEY');
            return reject();
        }
        /* Locale vars */
        let nonce = getNonce();
        let path = '/' + version + '/private/' + method;
        if(!post_data)
        {
            post_data+= '&nonce='+nonce;
        }
        else
        {
            post_data = 'nonce='+nonce;
        }

        let tosign = new jsSHA("SHA-256", "TEXT");
        tosign.update(nonce+''+post_data);
        let tosign_value = tosign.getHash("B64");

        let sign = new jsSHA("SHA-512", "B64");
        sign.setHMACKey(private_key, "B64");
        sign.update(btoa(path));
        sign.update(tosign_value);

        let sign_value = sign.getHMAC("B64");

        let xhr = new XMLHttpRequest;
        xhr.open( "POST", url+path, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.addEventListener("error", reject);
        xhr.addEventListener("load", function(){
            try
            {
                ok(JSON.parse(this.responseText));
            }
            catch(err)
            {
                reject();
            }
        });
        xhr.setRequestHeader('API-Key', api_key);
        xhr.setRequestHeader('API-Sign', sign_value);
        xhr.send(post_data);
    });
}

function QueryPublic(method, post_data)
{
    return new Promise(function(ok, reject)
    {
        /* Locale vars */
        let nonce = getNonce();
        let path = '/' + version + '/public/' + method;

        let xhr = new XMLHttpRequest;
        xhr.open( "GET", url+path+'?'+post_data, true);
        xhr.addEventListener("error", reject);
        xhr.addEventListener("load", function(){
            try
            {
                ok(JSON.parse(this.responseText));
            }
            catch(err)
            {
                reject();
            }
        });
        xhr.send(post_data);
    });
}
async function getCurrencyValue(key)
{
    if(key == currency)
    {
        return 1;
    }
    var ckey = currency_keys[key+currency];
    if(!ckey)
    {
        ckey = currency_keys[key+'XXBT'];
    }
    key = ckey;
    let r  = await QueryPublic('Ticker','pair='+key);
    if(r && r.result && r.result[key])
    {
        currency_values[key] = r.result[key].c[0];
    }

    var hour_seconds  = 3600;
    var day_seconds  = 86400;
    var week_seconds  = 604800;

    // Fetch data from last 24h and 1 week
    r  = await QueryPublic('Trades','pair='+key+'&since='+(server_time-hour_seconds)+'000000000');
    if(r && r.result && r.result[key])
    {
        last_hour[key] = r.result[key][0][0];
    }

    r  = await QueryPublic('Trades','pair='+key+'&since='+(server_time-day_seconds)+'000000000');
    if(r && r.result && r.result[key])
    {
        last_day[key] = r.result[key][0][0];
    }

    r  = await QueryPublic('Trades','pair='+key+'&since='+(server_time-week_seconds)+'000000000');
    if(r && r.result && r.result[key])
    {
        last_week[key] = r.result[key][0][0];
    }
    console.log('received',key, last_day, last_week);

    return 0;
}

async function run()
{
    total_hour = 0;
    total_day = 0;
    total_week = 0;
    let pairs  = await QueryPublic('AssetPairs','');
    for(var key in pairs.result)
    {
        let asset = pairs.result[key];
        if(asset.altname.indexOf('.d')==-1)
        {
            currency_keys[asset.base+asset.quote] = key;
        }
    }


    last_update = new Date();
    await getConf();
    configured = private_key && currency && api_key;


    // Get server time
    let r  = await QueryPublic('Time','');
    if(r && r.result && r.result.unixtime)
    {
        console.log('server itme updated');
        server_time = r.result.unixtime;
    }
    console.log('server time ',server_time);

    await getCurrencyValue('XXBT');

    let r_balance = await QueryPrivate('Balance');
    if(r_balance.result)
    {
        for(var key in r_balance.result) {
            if (r_balance.result.hasOwnProperty(key)) {
                var value = r_balance.result[key];
                balance[key] = r_balance.result[key];
                // Do not fetch data if irrelevant amount owned
                if(balance[key]>0.000001)
                {
                    await getCurrencyValue(key);
                }
            }
        }
    }

    // Sum all
    total = 0;
    for (var i in balance)
    {
        var key = currency_keys[i+currency];
        var is_bitcoin_relative = false;
        if(!key)
        {
            key = currency_keys[i+'XXBT'];
            is_bitcoin_relative = true;
        }
        if(last_hour[key])
        {
            var current = balance[i] * last_hour[key];
            if(is_bitcoin_relative)
            {
                total_hour +=  current * last_day['XXBT'+currency];
            }
            else
            {
                total_hour +=  current;
            }
        }
        if(last_day[key])
        {
            var current = balance[i] * last_day[key];
            if(is_bitcoin_relative)
            {
                total_day +=  current * last_day['XXBT'+currency];
            }
            else
            {
                total_day +=  current;
            }
        }
        if(last_week[key])
        {
            var current = balance[i] * last_week[key];
            if(is_bitcoin_relative)
            {
                total_week +=  current * last_week['XXBT'+currency];
            }
            else
            {
                total_week +=  current;
            }
        }
        if(currency_values[key])
        {
            var current = balance[i] * (currency_values[key] || 0);
            if(is_bitcoin_relative)
            {
                total +=  current * currency_values['XXBT'+currency];
            }
            else
            {
                total +=  current;
            }
        }
    }

    var text = '';
    var color = total>total_day ? '#4e9b4e' : '#e12b2b';
    if(!hide_number)
    {
        if(tickervalue=='diffhour')
        {
            color = total>total_hour ? '#4e9b4e' : '#e12b2b';
            text=((-1+(total/total_hour))*100).toFixed(2)+'%';
        }
        else if(tickervalue=='diffday')
        {
            color = total>total_day ? '#4e9b4e' : '#e12b2b';
            text=((-1+(total/total_day))*100).toFixed(2)+'%';
        }
        else if(tickervalue=='diffweek')
        {
            color = total>total_week ? '#4e9b4e' : '#e12b2b';
            text=((-1+(total/total_week))*100).toFixed(2)+'%';
        }
        else
        {
            text=total.toFixed(2)+' '+currency;
        }
    }
    browser.browserAction.setBadgeBackgroundColor({color:color});
    browser.browserAction.setBadgeText({text:text});
    browser.browserAction.setTitle({title:text});

    if(popup && popup.refresh)
    {
        popup.refresh();
    }
}


run();

