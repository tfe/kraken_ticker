var browser = browser || chrome;

function saveOptions(e) {
  e.preventDefault();
  browser.storage.local.set({
    api_key: document.querySelector("#api_key").value,
    private_key: document.querySelector("#private_key").value,
    currency: document.querySelector("#currency").value,
    tickervalue: document.querySelector("#tickervalue").value,
    hide_number: document.querySelector("#hide_number").checked
  }, function() {
    document.querySelector('.saved').classList.add('visible');
    });

  if(document.querySelector("#hide_number").checked)
  {
    browser.browserAction.setBadgeText({text:''});
  }
  window.setTimeout(function()
  {
      document.querySelector('.saved').classList.remove('visible')
  }, 5000);

  getBgPage().then(function(x)
  {
      x.run();
  });
}

function restoreOptions() {

  function setCurrentChoice(result, key) {
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  var getting = getStorage("api_key").then(function(result)
    {
        document.querySelector("#api_key").value = result.api_key || "";
    });

  var getting2 = getStorage("private_key").then(function(result)
    {
        document.querySelector("#private_key").value = result.private_key || "";
    });

  var getting3 = getStorage("currency").then(function(result)
    {
        document.querySelector("#currency").value = result.currency || "ZEUR";
    });
  var getting4 = getStorage("hide_number").then(function(result)
    {
        document.querySelector("#hide_number").checkbox = result.hide_number;
    });
  var getting5 = getStorage("tickervalue").then(function(result)
    {
        document.querySelector("#tickervalue").value = result.tickervalue || "balance";
    });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
